use clap::Parser;
use std::path::Path;

#[derive(Parser)]
#[command(author = "Yie007", version = "v1.0", about = "A simple tool thats displays the file sturcture using a tree structure", long_about = None)]
struct _Args {
    /// directory or file path
    path: Option<String>,
    /// maximum number of display layers
    #[arg(long, short)]
    limit: Option<String>,
    /// directory only
    #[arg(long = "dir", short)]
    dir_only: bool,
    /// show size
    #[arg(long = "size", short)]
    show_size: bool,
}

#[derive(Debug)]
pub struct Args {
    pub path: String,
    pub limit: u8,
    pub dir_only: bool,
    pub show_size: bool,
}

impl Args {
    pub fn build() -> Result<Args, &'static str> {
        let args = _Args::parse();
        // 解析path参数
        let path = args.parse_path()?;
        // 解析limit参数
        let limit = args.parse_limit()?;
        Ok(Args {
            path,
            limit,
            dir_only: args.dir_only,
            show_size: args.show_size,
        })
    }
}

impl _Args {
    // 解析path参数
    fn parse_path(&self) -> Result<String, &'static str> {
        // 考虑是否存在这个参数
        let path = &self.path;
        match path {
            Some(path) => {
                // 考虑path是否合法
                let true_path = Path::new(path);
                if true_path.exists() {
                    Ok(path.to_string())
                } else {
                    Err("file or directory not exist")
                }
            }
            None => Err("missing path"),
        }
    }

    // 解析limit参数
    fn parse_limit(&self) -> Result<u8, &'static str> {
        // 考虑是否存在这个参数
        let limit = &self.limit;
        match limit {
            Some(limit) => match limit.parse::<u8>() {
                // 考虑这个参数是不是u8类型
                Ok(limit) => {
                    // 考虑limit是否合法，要大于0
                    if limit < 1 {
                        Err("`limit` should be greater than 0")
                    } else {
                        Ok(limit)
                    }
                }
                Err(_) => Err("`limit` should be u8"),
            },
            None => Ok(u8::MAX),
        }
    }
}
