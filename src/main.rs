use rtree::Runner;
use std::process;
fn main() {
    let runner = Runner::build().unwrap_or_else(|err| {
        println!("Problem parsing parameters: {}", err);
        process::exit(1);
    });

    if let Err(err) = runner.run() {
        println!("Problem running program: {}", err);
        process::exit(1);
    };
}
