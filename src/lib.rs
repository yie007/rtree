use parser::Args;
use std::{collections::HashMap, path::Path};
mod parser;

pub struct Runner {
    args: Args,
}

// 记录文件夹大小的哈希表，用于优化计算size的递归
static mut FOLDER_SIZES: Option<HashMap<String, u64>> = None;

impl Runner {
    // 对外的接口
    pub fn run(&self) -> Result<(), &'static str> {
        match self.args.dir_only {
            true => Self::print_dir_tree(&self.args.path, 1, self.args.limit, self.args.show_size),
            false => Self::print_tree(&self.args.path, 1, self.args.limit, self.args.show_size),
        }
    }

    // 对外的接口
    pub fn build() -> Result<Runner, &'static str> {
        // 初始化FOLDER_SIZES
        unsafe { FOLDER_SIZES = Some(HashMap::new()) };
        // 构建结构体
        let args = Args::build()?;
        Ok(Runner { args })
    }

    // 递归函数，deep与显示有关，从1开始
    fn print_tree(path: &str, deep: u8, limit: u8, show_size: bool) -> Result<(), &'static str> {
        // 根据要求看是否显示大小
        let name = match show_size {
            true => Self::size_and_name(path),
            false => Self::only_name(path),
        }?;
        let path = Path::new(path);
        // 与显示相关
        if deep == 1 {
            println!("| -- {}", name);
        } else {
            let mut whitespace = String::new();
            // 空格数应为 (deep-1)*5 - 1
            for _i in 0..(deep - 1) * 5 - 1 {
                whitespace.push_str(" ");
            }
            println!("|{}| -- {}", whitespace, name);
        }

        // 递归子目录
        if path.is_dir() {
            if deep < limit {
                for entry in path.read_dir().map_err(|_| "iterator error")? {
                    let entry = entry.map_err(|_| "`DirEntry` error")?;
                    // 目录下每一项都递归
                    Self::print_tree(
                        entry.path().to_str().ok_or("path UTF-8 invalid")?,
                        deep + 1,
                        limit,
                        show_size,
                    )?;
                }
            } else {
                // 到达限制深度，隐藏目录下内容
                let mut whitespace = String::new();
                // 空格数应为 (deep - 1 + 1)*5 - 1
                for _i in 0..deep * 5 - 1 {
                    whitespace.push_str(" ");
                }
                println!("|{}| -- {}", whitespace, "hidden...");
            }
        }
        Ok(())
    }

    // 递归函数，只显示目录
    fn print_dir_tree(
        path: &str,
        deep: u8,
        limit: u8,
        show_size: bool,
    ) -> Result<(), &'static str> {
        // 根据要求看是否显示大小
        let name = match show_size {
            true => Self::size_and_name(path),
            false => Self::only_name(path),
        }?;
        let path = Path::new(path);
        if !path.is_dir() {
            return Ok(());
        }
        // 与显示相关
        if deep == 1 {
            println!("| -- {}", name);
        } else {
            let mut whitespace = String::new();
            // 空格数应为 (deep-1)*5 - 1
            for _i in 0..(deep - 1) * 5 - 1 {
                whitespace.push_str(" ");
            }
            println!("|{}| -- {}", whitespace, name);
        }

        // 递归子目录
        if path.is_dir() {
            if deep < limit {
                for entry in path.read_dir().map_err(|_| "iterator error")? {
                    let entry = entry.map_err(|_| "`DirEntry` error")?;
                    // 目录下每一项都递归
                    Self::print_dir_tree(
                        entry.path().to_str().ok_or("path UTF-8 invalid")?,
                        deep + 1,
                        limit,
                        show_size,
                    )?;
                }
            } else {
                // 到达限制深度，在存在子目录的时候才显示hidden，若全是文件则什么都不做
                for entry in path.read_dir().map_err(|_| "iterator error")? {
                    let entry = entry.map_err(|_| "`DirEntry` error")?;
                    // 如果目录下存在子目录，那么显示hidden
                    if entry.path().is_dir() {
                        let mut whitespace = String::new();
                        // 空格数应为 (deep - 1 + 1)*5 - 1
                        for _i in 0..deep * 5 - 1 {
                            whitespace.push_str(" ");
                        }
                        println!("|{}| -- {}", whitespace, "hidden...");
                        break;
                    }
                }
            }
        }
        Ok(())
    }

    // 获取大小及名称
    fn size_and_name(p: &str) -> Result<String, &'static str> {
        let path = Path::new(p);
        let metadata = path.metadata().map_err(|_| "metadata call failed")?;
        let name = path
            .file_name()
            .ok_or("path terminates in `..`")?
            .to_str()
            .ok_or("path UTF-8 invalid")?;

        let size = match metadata.is_dir() {
            // 如果是dir，调用函数计算其大小
            true => {
                let folder_sizes = unsafe { FOLDER_SIZES.as_mut().unwrap() };
                Self::get_folder_size(p, folder_sizes)?
            }
            false => metadata.len(),
        } as f64;
        // 根据不同大小更换显示单位
        if size < 10000.0 {
            Ok(format!("[{} Bytes] {}", size, name))
        } else {
            let size_kb = size / 1024.0;
            if size_kb < 10000.0 {
                Ok(format!("[{:.2} KB] {}", size_kb, name))
            } else {
                let size_mb = size_kb / 1024.0;
                Ok(format!("[{:.2} MB] {}", size_mb, name))
            }
        }
    }

    // 只获取名称
    fn only_name(p: &str) -> Result<String, &'static str> {
        let path = Path::new(p);
        let name = path
            .file_name()
            .ok_or("path terminates in `..`")?
            .to_str()
            .ok_or("path UTF-8 invalid")?;
        Ok(String::from(name))
    }

    // 获取文件夹大小，为其下所有文件大小之和，使用全局哈希表减少重复计算
    fn get_folder_size(
        p: &str,
        folder_sizes: &mut HashMap<String, u64>,
    ) -> Result<u64, &'static str> {
        // 看看哈希表里有无记录
        if let Some(size) = folder_sizes.get(p) {
            return Ok(*size);
        }
        // 没有就进行求和计算
        let mut total_size = 0u64;
        let path = Path::new(p);
        for entry in path.read_dir().map_err(|_| "iterator error")? {
            let entry = entry.map_err(|_| "`DirEntry` error")?;
            let entry_metadata = entry.metadata().map_err(|_| "metadata call failed")?;
            // 如果是dir就递归
            if !entry_metadata.is_dir() {
                total_size += entry_metadata.len();
            } else {
                let entry_path = entry.path();
                let entry_path = entry_path.to_str().ok_or("path UTF-8 invalid")?;
                total_size += Self::get_folder_size(entry_path, folder_sizes)?;
            }
        }
        // 记录到哈希表中
        folder_sizes.insert(p.to_string(), total_size);
        Ok(total_size)
    }
}
