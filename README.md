## 概述

**Rtree**是一个用Rust构建的命令行工具，功能是**以树形结构显示文件目录结构**

注：这是一个个人用的学习小项目，代码行数约为300行。

## 使用展示

### 使用说明

![](md-images/image-20240117162626267.png)

### 效果展示

1. 普通执行：

   ![](md-images/image-20240117163323706.png)

2. 限制层数：

   ![](md-images/image-20240117163507915.png)

3. 显示大小：

   ![](md-images/image-20240117164228537.png)

4. 只显示文件夹：

   ![](md-images/image-20240117164326389.png)

## 实现概述

1. 使用外部库`clap`作为命令行参数解析器，结构体`_Args`内拥有成员函数负责进一步解析参数

   ```rust
   #[derive(Parser)]
   #[command(author = "Yie007", version = "v1.0", about = "A simple tool thats displays the file sturcture using a tree structure", long_about = None)]
   struct _Args {
       /// directory or file path
       path: Option<String>,
       /// maximum number of display layers
       #[arg(long, short)]
       limit: Option<String>,
       /// directory only
       #[arg(long = "dir", short)]
       dir_only: bool,
       /// show size
       #[arg(long = "size", short)]
       show_size: bool,
   }
   
   impl _Args {
       // 解析path参数
       fn parse_path(&self) -> Result<String, &'static str> {
           // ...
       }
   
       // 解析limit参数
       fn parse_limit(&self) -> Result<u8, &'static str> {
           // ...
       }
   }
   ```


2. 使用结构体`Arg`包装已被验证过的参数，对外提供负责构建的`build()`函数

   ```rust
   pub struct Args {
       pub path: String,
       pub limit: u8,
       pub dir_only: bool,
       pub show_size: bool,
   }
   
   impl Args {
       pub fn build() -> Result<Args, &'static str> {
           // ...
       }
   }
   ```

3. 使用一个`Runner`类，对外提供`build()`和`run()`

   ```rust
   pub struct Runner {
       args: Args,
   }
   
   impl Runner {
       // 对外的接口
       pub fn run(&self) -> Result<(), &'static str> {
           // 一些运行逻辑...
       }
   
       // 对外的接口
       pub fn build() -> Result<Runner, &'static str> {
           // 构建结构体等...
       }
   }
   ```

4. 核心递归函数及一些辅助函数

   ```rust
   impl Runner {
       // 递归函数，deep与显示有关，从1开始
       fn print_tree(path: &str, deep: u8, limit: u8, show_size: bool) -> Result<(), &'static str> {
           // ...
       }
   
       // 递归函数，只显示目录
       fn print_dir_tree(
           path: &str,
           deep: u8,
           limit: u8,
           show_size: bool,
       ) -> Result<(), &'static str> {
           // ...
       }
   
       // 获取大小及名称
       fn size_and_name(p: &str) -> Result<String, &'static str> {
           // ...
       }
   
       // 只获取名称
       fn only_name(p: &str) -> Result<String, &'static str> {
           // ...
       }
   
       // 获取文件夹大小，为其下所有文件大小之和，使用全局哈希表减少重复计算
       fn get_folder_size(
           p: &str,
           folder_sizes: &mut HashMap<String, u64>,
       ) -> Result<u64, &'static str> {
           // ...
       }
   }
   ```

5. `main()`函数负责处理错误

   ```rust
   fn main() {
       let runner = Runner::build().unwrap_or_else(|err| {
           println!("Problem parsing parameters: {}", err);
           process::exit(1);
       });
   
       if let Err(err) = runner.run() {
           println!("Problem running program: {}", err);
           process::exit(1);
       };
   }
   ```

   



